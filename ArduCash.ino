/* ArduCASH
 * An Arduino register cash.
 *
 * Copyright 2021 by Jaume Batlle Brugal
 * 
 * This file is part of ArduCASH.
 * 
 * ArduCASH is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * ArduCASH is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ArduCASH. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ArduCASH is a scketch for the Arduino platform that implements a
 * cash register with records for up to 50 clients. Each client has a
 * name field of 15 characters, and a value field which holds the client's
 * balance.
 * 
 * It features a calculator (simple, only add and subtract) and two
 * programmable keys.
 *
 *  +----------------+ -+ VMA203 shield 
 *  |ArduCASH        |  |
 *  |(c)2021 J.Batlle|  |
 *  +----------------+  |
 *        [^]           |
 *  [S][<]   [>][R]     |
 *        [v]          -+
 *  +---+---+---+---+  -- 4x4 matrix keypad
 *  | 1 | 2 | 3 | + | <- '+' key is labelled as '+/='
 *  +---+---+---+---+
 *  | 4 | 5 | 6 | - |
 *  +---+---+---+---+
 *  | 7 | 8 | 9 | C |
 *  +---+---+---+---+
 *  | P | 0 | Q | = | <- P is labelled as 'P1' and Q as 'P2'
 *  +---+---+---+---+    = is labelled as an 'intro' arrow (<-')
 */

#include <Key.h>
#include <Keypad.h>
#include <LiquidCrystal.h>

// Helper functions and definitons for pins and constants.
#include "ArduCashLib.h"

// Physical part: keypads, buttons and lcd
#include "CashBox.h"

// Logical part: list of clients management
#include "ClientList.h"


// Keypad setup

const char keypadRowPins[KEYPAD_NUM_ROWS] = {KPROW1, KPROW2, KPROW3, KPROW4};
const char keypadColPins[KEYPAD_NUM_COLS] = {KPCOL1, KPCOL2, KPCOL3, KPCOL4};

byte keys[KEYPAD_NUM_ROWS][KEYPAD_NUM_COLS] = {
  {'1', '2', '3', '+'},
  {'4', '5', '6', '-'},
  {'7', '8', '9', 'C'},
  {'P', '0', 'Q', '='},
};


// Global variables

byte brightness = 95; // Backlight brightnes (0..255)

LiquidCrystal lcd(RS, EN, DB4, DB5, DB6, DB7);

Keypad pad( makeKeymap(keys),
            keypadRowPins,
            keypadColPins,
            KEYPAD_NUM_ROWS,
            KEYPAD_NUM_COLS);

CashBox box(&pad, BTN_SENSOR, &lcd); // Manages IO and hardware
ClientList list; // Program logic for client's list


void setup()
{
  // LCD screen backlight
  pinMode(BACKLIGHT, OUTPUT);
  analogWrite(BACKLIGHT, brightness);

  Serial.begin(9600);

  box.print("ArduCASH v1.06", "(C)2021 J.Batlle");

  delay(2000);
    
  if (readEEPROM(list, box) < 1) {
    box.printf(2, "<empty>");
    
  } else {
    // Show first
    box.printf(2, "%c%s", list.mark(), list.name());
  }
}

void loop()
{
  byte key;

  key = box.readKey(); 

  /* Sure? /
  // If list is empty, skip all key testing.
  if (list.isEmpty()) {
    key = NO_KEY;
  }
  // */
  
  switch (key) {
    case NO_KEY: // Most usual case, evaluate first.
      break;

    case DOWN:
    case '-':
      box.print(1, list.name());
      list.forward();
      box.printf(2, "%c%s", list.mark(), list.name()); 
      break;

    case UP:
    case '+':
      box.print(2, list.name());
      list.backward();
      box.printf(1, "%c%s", list.mark(), list.name());
      break;
      
    case '=': // Calculate new price.
      box.print(1, list.name());
     
      list.setValue(box.calculator(list.value()));
      saveValue(list);
      list.setMark(CL_STATUS_EDITED);

    case SELECT: 
      // Print calculated result on screen:

      if (list.mark() != CL_STATUS_EDITED) {
        list.setMark(CL_STATUS_VISITED);
      }
      
      box.printf(1, "%c%s", list.mark(), list.name());
      box.printf(2, ResultFmt,
                      (list.value() < 0) && (list.value() > -100) ? '-' : ' ',     
                      list.euros(),
                      list.cents());
      break;

    case 'C':  // Simple calculator, not attached to a user
      box.printf(1, "Calculadora:");
      box.calculator(0);
      box.printf(1, "Resultat:");
      break;

    case 'P':
      box.printf(1, PNewValueFmt, '1'); // Enter P1 value
      box.p1 = box.calculator(0); // If op is cancelled, P1 will be set to 0.
      box.printf(2, ResultFmt,
                      (box.p1 < 0) && (box.p1 > -100) ? '-' : ' ',     
                      box.p1 / 100,
                      abs(box.p1) % 100);
      saveValue(1, box.p1); // store new value
      break;

    case 'Q':
      box.printf(1, PNewValueFmt, '2'); // Enter P2 value
      box.p2 = box.calculator(0); // Same as for P1
      box.printf(2, ResultFmt,
                      (box.p2 < 0) && (box.p2 > -100) ? '-' : ' ',     
                      box.p2 / 100,
                      abs(box.p2) % 100);
      saveValue(2, box.p2); // store new value
      break;

    case LEFT:
      brightness -= 10;
      analogWrite(BACKLIGHT, brightness);
      break;      

    case RIGHT:
      brightness += 10;
      analogWrite(BACKLIGHT, brightness);
      break;
        
    default: // Do nothing
        ;
  }
}

void serialEvent()
{
  if (Serial.available()) { // is serialEvent called _only_ when Serial.available()?
    const char msg[] = "nt dades";
    char command = Serial.read();

    // 'B' stands for 'backup', 'R' for 'restore', 'D' for 'dump'.
    switch (command) {
      case 'B':
        // Sends a CSV with client list.
        box.printf(1, "Envia%s", msg);
        backup(list);
        delay(500);
        box.print(2, DoneMsg);
        //delay(500);
        break;
        
      case 'R':
        // Reads CSV input and overwrites list
        box.printf(1, "Rebe%s", msg);
        
        if (restore(list)) {
          box.print(2, DoneMsg);
          delay(100);
        } else {
          box.printf(2, "Error!");
          
          // Accessing EEPROM contents  
          if (readEEPROM(list, box) < 1) {
            box.printf(2, "<empty>");
              
          } else {
            // Show first
            box.printf(2, "%c%s", list.mark(), list.name());
          }
        }
        //delay(500);
        break;

      case '\n':
        // In case Serial monitor sends it, just ignore.
        break;

      case 'D':
        dumpEEPROM(0, 1024);
        break;
        
      default:
        if (DBG) Serial.println(F("Ordre desconeguda."));
    }
  }
}
