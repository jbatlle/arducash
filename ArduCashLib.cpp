/* ArduCashLib.cpp
 *  
 * Copyright 2021 by Jaume Batlle Brugal
 * 
 * This file is part of ArduCASH.
 * 
 * ArduCASH is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * ArduCASH is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ArduCASH. If not, see <http://www.gnu.org/licenses/>.
 * 
*/

#include <Arduino.h>
#include "ArduCashLib.h"

// Helper functions

int validEEPROMData()
{
  int data;
  EEPROM.get(ADDR_PWD, data);

  if (data == ROM_PWD) {
    EEPROM.get(ADDR_VER, data);
  }

  return data == VERSION;
}


int readEEPROM(ClientList &cl, CashBox &box)
{
  // fresh start
  
  while (!cl.isEmpty()) {
    cl.delItem();
  }

  box.p1 = box.p2 = 0;

  if (validEEPROMData()) {

    // Read P1 and P2 values  
    EEPROM.get(ADDR_P1, box.p1);
    EEPROM.get(ADDR_P2, box.p2);
      
    // Read number of items in list
    uint8_t to;
    EEPROM.get(ADDR_LIST, to);
  
    // Load list items
    ClientItem item;

    // Only part of ClientItem info is stored in EEPROM
    int item_size = sizeof(ClientItem::name) + sizeof(ClientItem::value);
    
    for (uint8_t i = 0; i < to; i++) {
      EEPROM.get(item_size * i + ADDR_LIST + 1, item);
      cl.addItem(item.name, item.value);
    }
  }  

  cl.reset();
  return cl.items();
}


void saveValue(ClientList &cl)
{
  unsigned int address;

  address = ((sizeof(ClientItem::name) + sizeof(ClientItem::value)) * cl.current()) 
              + sizeof(ClientItem::name) 
              + ADDR_LIST + 1;

  EEPROM.put(address, cl.value());
}


// P<button>'s new value.
void saveValue(uint8_t button, long value)
{
  unsigned address;

  if (button == 1) {
    address = ADDR_P1;
    
  } else if (button == 2) {
    address = ADDR_P2;
    
  } else {
    return; // Invalid button value, do nothing;
  }

  EEPROM.put(address, value);
}


void backup(const ClientList &cl)
{
  char line[25];
  char sign[2] = {'\0', '\0'};

  cl.reset();

  for (uint8_t i = 0; i < cl.items(); i++) {
    if (cl.value() < 0) {
      sign[0] = '-';
    } else {
      sign[0] = '\0';
    }
    
    sprintf(line, "%s,%s%li.%02li\n", 
            cl.name(), sign, abs(cl.euros()), cl.cents());
    Serial.print(line);
    cl.forward();
  }
  
  // Backup tool has a timeout, so when nothing else is sent, it knows
  // it's all done.
}


int restore(ClientList &cl)
{
  // Writing to EEPROM is slow and interferes with Serial reading,
  // so we must first read all data from Serial, and when it's done,
  // write it to EEPROM
  //
  String usname;
  long money;
  bool error = false;
  bool done = false;

  // Empty cl first.
  while (!cl.isEmpty()) {
    cl.delItem();
  }

  // Skip blanks
  while ((Serial.peek() == '\x0A') || (Serial.peek() == '\x0D'))
  {
    Serial.read();
  }

  while (!done && !error) {
    unsigned long timeStamp;

    timeStamp = millis();
    while (!done && !Serial.available()) {
      // Waits 0.5s for data
      if (millis() - timeStamp > 500) {
          done = true;
      }
    }

    if (!done) {      
      // Expected "<char[15]>,<int>.<int>\n"
      usname = Serial.readStringUntil(',');
      money = Serial.parseInt(SKIP_ALL, '.');

      // Skip EOL
      while ((Serial.peek() == '\x0A')
            || (Serial.peek() == '\x0D')
            || (Serial.peek() == ' '))
      {
        Serial.read();
      }
  
      if (usname.length() > 0) {
        // Add user
        ClientItem item;
        
        usname.toCharArray(item.name, CLIENT_NAME_LENGTH + 1);
        item.value = money;
  
        // Add to list
        if (!cl.addItem(item.name, item.value)) {
          Serial.println(F("WARNING: List is already full. Discarding next entries."));
          done = true;
        }
  
      } else {
        Serial.println(F("ERROR: Invalid user name."));
        error =  true;
      }
    }
  }

  if (!error) {   
    ClientItem item;

    // Init EEPROM in case it was not done before, so we know
    // that there's valuable data in there.
    EEPROM.put(ADDR_PWD, ROM_PWD); // EEPROM password
    EEPROM.put(ADDR_VER, VERSION); // EEPROM storage version

    cl.reset();
    unsigned address = ADDR_LIST + 1;
    
    for (int8_t i = 0; i < cl.items(); i++) {
      strcpy(item.name, cl.name());
      EEPROM.put(address, item.name);
      address += CLIENT_NAME_LENGTH + 1;
      
      item.value = cl.value();
      EEPROM.put(address, item.value);
      address += sizeof(item.value);
      
      cl.forward();
    } 

    // Save number of items written in EEPROM
    EEPROM.update (ADDR_LIST, byte(cl.items()));
  }
  
  cl.reset();
  return !error;
}


// Debugging functions

void dumpEEPROM(unsigned int from = 0, unsigned int to = 5000)
{
  // Show EEPROM contents in a somewhat readable format
  
  char out[10];
  int tabcount = 0;
  byte b;
  char c;
  int i = from;
  int j = EEPROM.length();

  if (to < j) {
    j = to;
  }
  
  for (i = from ; i < j; i++) { // if i > j won't print a thing.
    b = EEPROM.read(i);

    if (b == 0) {
      c = '0';
    } else if (b == 32) {
      c = ' ';
    } else if ((b < 128) && (b >= 34)) {
      c = b;
    } else {
      c = '.';
    }
    
    sprintf( out, "%04X:%02X %c %s", 
              i, b, c,
              (((tabcount + 1) % 7)) ? "" : "\n"
    );
    Serial.print(out);
  
    tabcount++;
  }

  Serial.println();
}
