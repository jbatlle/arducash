/* ArduCashLib.h
 *  
 * Copyright 2021 by Jaume Batlle Brugal
 * 
 * This file is part of ArduCASH.
 * 
 * ArduCASH is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * ArduCASH is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ArduCASH. If not, see <http://www.gnu.org/licenses/>.
 * 
*/

#ifndef _ARDUCASHLIB_H
#define _ARDUCASHLIB_H

#include <EEPROM.h>
#include <Key.h>
#include <Keypad.h>
#include <LiquidCrystal.h>

#include "ClientList.h"
#include "CashBox.h"


// Constants definitions

// Debug enabler (or disabler if 0)
#define DBG 0

// EEPROM "password": allows us to know that there's something
// usable written there. Can you read "CASH"?
#define ROM_PWD 0xCA54

// Version of EEPROM store format
#define VERSION 0x0100

// EEPROM will store our values in fixed places:
#define ADDR_PWD 0x00
#define ADDR_VER 0x02
#define ADDR_P1  0x04
#define ADDR_P2  0x08
#define ADDR_LIST 0x0C

// Pin layout for the keypad:
// A matrix keypad of 4x4 keys, 8 pins required.
#define KEYPAD_NUM_COLS byte(4)
#define KEYPAD_NUM_ROWS byte(4)

#define KPROW1  2
#define KPROW2  3
#define KPROW3  11
#define KPROW4  12

#define KPCOL1  A1
#define KPCOL2  A2
#define KPCOL3  A3
#define KPCOL4  A4

// Pin layout for the screen:
// Using VMA203 shield:
#define BTN_SENSOR  A0
#define DB4         4
#define DB5         5
#define DB6         6
#define DB7         7
#define RS          8
#define EN          9
#define BACKLIGHT   10

// String constants
// Detect any string used more than once, and declare it here
// to save some bytes
const char DoneMsg[] = "Fet."; 
const char PNewValueFmt[] = "Nou valor de P%c";
const char ResultFmt[] = "%c%ld.%02ld";

// Helper functions

// Read contents of EEPROM and places them in cl
// Returns the number of items red.
int readEEPROM(ClientList &cl, CashBox &box);

// Save current client Item's value to EEPROM
// Writing is done only if value has changed
void saveValue(ClientList &cl);

// The same, but for P1 or P2 values
// button is either 1 or 2, which P button value's to save
// Value is P<button>'s new value.
void saveValue(uint8_t button, long value);

// Sends through Serial a CSV list of names and values in 'cl'
// CSV format: '<name>,[-]<euro>.<cent>\n'
void backup(const ClientList &cl);

// Reads a CSV list through Serial of name and value pairs, and
// *overwrites* 'cl' with them.
// CSV format: '<name>,[-]<euro>.<cent>\n'
// Returns non-zero on success, zero (false) otherwise.
int restore(ClientList &cl);

// Debugging functions
void dumpEEPROM(unsigned int from = 0, unsigned int to = 5000);

#endif
