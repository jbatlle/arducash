/* CashBox.cpp
 *  
 * Copyright 2021 by Jaume Batlle Brugal
 * 
 * This file is part of ArduCASH.
 * 
 * ArduCASH is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * ArduCASH is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ArduCASH. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CashBox.h"

const char MoneyFormat[] = "%12ld.%02ld%c";

CashBox::CashBox(Keypad * in, int8_t sensor, LiquidCrystal * out)
{
  _input = in;
  _btn_sensor = sensor;
  _output = out;
  _output->begin(16,2);
  p1 = p2 = 0;  
  _lastKey = NO_KEY;
  _keyTime = 0;
}

    
// Show text on the LCD screen, either 1 or two lines
void CashBox::print(char *line1, char *line2 = (char *) NULL)
{
  char text[17];
  
  if (line1 != NULL) {
    snprintf(text, 17, "%s", line1);
  }
  _show(1, text);

  if (line2 != NULL) {
    snprintf(text, 17, "%s", line2);
  }
  _show(2, text);
}


void CashBox::print(int8_t lnum, char *text)
{
  if (text != NULL) {
    this->printf(lnum, "%s", text);
  }
}


void CashBox::printf(int8_t lnum, const char * format, ...)
{
  va_list args;
  char text[17];
 
  if ((0 < lnum) && (lnum < 3)) {
    va_start(args, format);
    vsnprintf(text, 17, format, args);
    va_end(args);
    _show(lnum, text);
  }
}


// text should be a valid C string up to 16 chars.
void CashBox::_show(int8_t lnum, const char *text)
{
    if ((lnum == 1) || (lnum == 2)) {
    // Erase the line
    _output->setCursor(0, lnum - 1);
    for (int8_t i = 0; i < 17; i++) {
      _output->write(' ');
    }
    
    // Write new line
    _output->setCursor(0, lnum - 1);
    _output->print(text);
  }
}


// Read a key from its inputs.
char CashBox::readKey()
{
  // Read VMA203 buttons and keypad ones
  
  int key = NO_KEY;
  int sensor = 1001; // Set to a value to 'clean' garbage

  // Keypad
  //
  if((key = _input->getKey()) == NO_KEY) {

    // No keypad key pressed. Test for shield buttons
    
    if ((_keyTime == 0) || (millis() - _keyTime > 250)) {
 
      // Debouncing time gone, read a new key      
 
      _keyTime = millis();
      sensor = analogRead(_btn_sensor);   // read the value from the sensor
   
      // Shield buttons are centered at these values: 0, 100, 260, 410, 640
      // add approx 50 to those values and check to see if we are close
    
      if (sensor > 1000) {
        key = NO_KEY;
        
      } else if (sensor < 50) {
        key = RIGHT;
        
      } else if (sensor < 150) {
        key = UP;
        
      } else if (sensor < 310) {
        key = DOWN;
        
      } else if (sensor < 465) {
        key = LEFT;
        
      } else if (sensor < 710) {
        key = SELECT;
      }
    }
  }

  if (key != NO_KEY) {
    _lastKey = key;
  }
  
  return key;
}

    
// Perform a calculation on screen.
// Returns result.
// If operation is cancelled, returns init_value.
// If 'SELECT' is pressed, returns 0
//
long CashBox::calculator(long init_value)
{
  /* Calculator uses only line 2 of the screen. At every new calculus, 
   * line 2 is replaced by the total amount.
   */
  char c = NO_KEY;
  long acum;

  acum = init_value;

  // Print current balance
  this->printf(2, MoneyFormat,
               abs(acum) / 100,
               abs(acum) % 100,
               (acum < 0) ? '-' : ' ');
  
  while ((c != '=') && (c != 'C') && (c != SELECT)) {
    if (c == NO_KEY) {
      // keep reading keys
      c = readKey();

    } else {
            
      switch (c) {
        case 'P':
          acum += p1;
          this->printf(2, MoneyFormat,
                       abs(p1) / 100,
                       abs(p1) % 100,
                       p1 < 0 ? '-' : ' ');
                    
          delay(500); // briefly show added amount
          c = readKey();
          break;
          
        case 'Q':
          acum += p2;
          this->printf(2, MoneyFormat, 
                       abs(p2) / 100,
                       abs(p2) % 100,
                       p2 < 0 ? '-' : ' ');

          delay(500); // briefly show added amount
          c = readKey();
          break;
  
        case '+':
          acum += _getNum();
          c = _lastKey; // Need last key entered, not a new one.
          break;
  
        case '-':
          acum -= _getNum();
          c = _lastKey; // Need last key entered, not a new one.
          break;
  
        default:
          // Nothing to do with the key, inform and get a new one.
          this->print(2, "OK + - P1 P2 C");
          delay(650);
          c = readKey();
      }

      this->printf(2, MoneyFormat, abs(acum) / 100,
                      abs(acum) % 100, acum < 0 ? '-' : ' ');
    }
  }

  if (c == 'C') {
    // "Cancel"
    acum = init_value;
  } else if (c == SELECT) {
    // "Pays all"
    acum = 0;
  }
  
  return acum;
}


// Reads a number from keypad and shows it in the screen while it is beeing
// entered.
// Returns the number when a key other than a digit or 'P' or 'Q' is entered,
// returs 0 if 'C' is entered.
//
long CashBox::_getNum()
{
  bool done = false;
  bool negative = false;
  long number = 0;

  while (this->readKey() == NO_KEY) ; // Blocking key reading.

  if (_lastKey == '-') {
    // First minus sign is for a negative number
    negative = true;
    while (this->readKey() == NO_KEY) ; // Read next key
  }
  
  while (!done) {
          
    if ((_lastKey == '+')
       || (_lastKey == '=') 
       || (_lastKey == '-')
       || (_lastKey == SELECT))
    {
      done = true;
          
    } else if (_lastKey == 'C') {
      // It cancels number reading at once!
      number = 0;
      done =  true;

    } else if (('0' <= _lastKey) && (_lastKey <= '9')) {
      number = number * 10 + long(_lastKey - '0');
      
    } else if (_lastKey == 'P') {
      number = p1; // No add, just place its value!
      this->printf(2, MoneyFormat,
                   abs(number) / 100,
                   abs(number) % 100,
                   (number < 0) ? '-' : ' ');
                   
      while (this->readKey() == NO_KEY) ; // Read next command, please.

      done = true;
      
    } else if (_lastKey == 'Q') {
      number = p2;
      this->printf(2, MoneyFormat,
                   abs(number) / 100,
                   abs(number) % 100,
                   (number < 0) ? '-' : ' ');

      while (this->readKey() == NO_KEY) ; // Read next command, please.
            
      done = true;
    }

    this->printf(2, MoneyFormat,
                 abs(number) / 100,
                 abs(number) % 100,
                 negative ? '-' : ' ');
    
    if (!done) {
      while (this->readKey() == NO_KEY) ; // Blocking key reading.
    }
  }

  return number * (negative ? -1 : 1);
}
