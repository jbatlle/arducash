/* CashBox.h
 *  
 * Copyright 2021 by Jaume Batlle Brugal
 * 
 * This file is part of ArduCASH.
 * 
 * ArduCASH is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * ArduCASH is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ArduCASH. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CASHBOX_H
#define CASHBOX_H

/* CashBox
 *  
 *  Object to control inputs, outputs and operations on the phisical
 *  cash box object, including shield buttons, keypad and lcd screen.
 */

#include <Key.h>
#include <Keypad.h>
#include <LiquidCrystal.h>

// Button names for WMA203 shield:
#define SELECT  1
#define UP      2
#define DOWN    3
#define LEFT    4
#define RIGHT   5
// NO_KEY already defined in Key.h as 'const char NO_KEY = '\0';'


class CashBox {
  public:

    // Creates a CashBox object
    // 'input' is an instantiated and initialized Keypad object.
    // 'output' is an instantiated and initialized LiquidCrystal object
    // 'button_sensor' is the analog pin where values for the WMA203 shield
    //                 buttons are going to be red.
    CashBox(Keypad * input, int8_t button_sensor, LiquidCrystal * output);
    
    // Programmable keys. Directly accessible.
    long p1, p2;

    // Show text on the LCD screen, either 1 or two lines
    void print(char *line1, char *line2 = (char *) NULL);

    // Updates one line of lcd screen, either line 1 or line 2.
    // line = [1|2]
    void print(int8_t line, char *text);

    // The same, but formatted text!
    void printf(int8_t linenum, const char * format, ...);

    // Read a key from its inputs.
    char readKey();
    
    // Perform a calculation on screen. init_value is *added* to the calculus.
    // Returns result.
    // If operation is cancelled, returns init_value.
    long calculator(long init_value = 0);

  private:
    //char _line[2][17]; // Screen lines to show. Maybe don't need to be persistent...
    char _lastKey;
    int8_t _btn_sensor; // WMA203 button sensor pin
    unsigned long _keyTime = 0; // timestamp for debouncing

    Keypad * _input;
    LiquidCrystal * _output;

    // Updates LCD with the contents of line, either 1, 2 or both.
    // lnum <= 0: both; lnum 1 or 2 : line. Else, nothing.
    //void _show(int8_t lnum = 0);


    // Updates a single LCD line [1|2] with text.
    void _show(int8_t lnum, const char *text);


   // Reads a number from keypad and shows it in the screen while it is beeing
   // entered.
   // Returns the number red when '+', '-', '=' is entered,
   // returs 0 if 'C' is hit.
   long _getNum(); 
};

#endif
