/* ClientList.cpp
 * 
 * Copyright 2021 by Jaume Batlle Brugal
 * 
 * This file is part of ArduCASH.
 * 
 * ArduCASH is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * ArduCASH is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ArduCASH. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#include "ClientList.h"

ClientList::ClientList()
{
  _last = -1;
  _current = 0;
}


int8_t ClientList::current()
{
  if (_last > -1) { // list empty mark
    return _current;
  } else {
    return -1;
  }
}


void ClientList::forward()
{
  // This is safe for empty lists: _current is not used,
  // so you can assign it any value you want.

  if (++_current > _last) {
    _current = 0;
  }
}


void ClientList::backward()
{
  // This is safe for empty lists: _current is not used,
  // so you can assign it any value you want.
  
  if (_current == 0) {
    _current = _last; 
  } else {
    _current--;
  }
}


void ClientList::reset()
{
  // Set current client to first. If list is empty, do nothing.
  _current = 0; // it already was if list was empty.
}


uint8_t ClientList::items()
{
  if (_last == -1) {
    return 0;
  } else {
    return _last + 1; // could always be this case!
  }
}


long ClientList::value()
{ 
  if (_last > -1) {
    return _list[_current].value;
  } else {
    return 0; // List is empty, no value...
  }  
}


long ClientList::euros()
{
  return value() / 100;
}


long ClientList::cents()
{
  if (value() < 0) {
    return -1L * value() % 100;
  } else {
    return value() % 100;
  }
}


const char * ClientList::name()
{
  if (_last > -1) {
    return _list[_current].name;
  } else {
    return (void *) NULL; // Error mark. List is empty!
  }
}


char ClientList::mark()
{
  if (_last > -1) {
    return _list[_current].mark;
  } else {
    return '\0'; // no mark for empty lists
  }
}


void ClientList::setMark(char c)
{
  if ((c == CL_STATUS_UNVISITED) ||
      (c == CL_STATUS_VISITED) ||
      (c == CL_STATUS_EDITED))
  {
    _list[_current].mark = c;
  }
}

void ClientList::setValue(long num)
{
  _list[_current].value = num;
}



bool ClientList::isFull()
{
  return _last == (LIST_SIZE - 1);
}


bool ClientList::isEmpty()
{
  return _last == -1;
}


int ClientList::addItem(char * name, long value)
{
  // Insert new item in last position
  // added item becomes current item
  // Return 0 if list is full, and no insertion is done.

  if (_last == LIST_SIZE - 1) {
    return 0;
    
  } else {
    _last++;
    snprintf(_list[_last].name, CLIENT_NAME_LENGTH + 1, "%s", name);
    _list[_last].value = value;
    _list[_last].mark = CL_STATUS_UNVISITED;
    _current = _last;
    return 1;
  }  
}


int ClientList::delItem()
{
  if (_last == -1) { // nothing to delete
    return 0;
    
  } else { 
    for (int8_t i = _current; i < _last; i++) {
      // Use strcpy because stored names are equal size and always zero-ended.
      strcpy(_list[i].name, _list[i+1].name);
      _list[i].value =  _list[i+1].value;
    }
    _last--;

    // Deleting last item, _current can be out of bounds.
    //
    if ((_current > 0) && (_current > _last)) {
      _current = _last;
    }

    return 1;
  }
}
