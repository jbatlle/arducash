/* ClientList.h
 *  
 * Copyright 2021 by Jaume Batlle Brugal
 * 
 * This file is part of ArduCASH.
 * 
 * ArduCASH is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * ArduCASH is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ArduCASH. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CLIENTLIST_H
#define CLIENTLIST_H

#include <Arduino.h>

// List of consumers size. (Maybe SRAM marks the limit, not EEPROM!)
#define LIST_SIZE 50

#define CLIENT_NAME_LENGTH 15

// Status marks used to show for each client in list. Defaults to
// CL_STATUS_UNVISITED on every restart, so:
//   - are marks according to session.
//   - there's no need to store them in EEPROM: live only in RAM
//
#define CL_STATUS_UNVISITED '>'
#define CL_STATUS_VISITED   '*'
#define CL_STATUS_EDITED    '#'

// ClientList holds a client list of name and value.
// Its size must be smaller than EEPROM.lenght() - 13, because we are
// using some extra bytes to store app password, etc.
//
// For the UNO:
// ------------
// list == 20 x LIST_SIZE bytes (B)
// p1 + p2 == 8 B
// actual-list-length = 1 B
// Total: 20 B x 50 + 8 B + 1 B = 1009 B. So, LIST_SIZE can be 50 to fit in 1024 B.
//

struct ClientItem {
  char name[CLIENT_NAME_LENGTH + 1];
  long value;
  char mark;
};


class ClientList {
  public:
    ClientList();
    
    // List navigation
    
    int8_t current();
    // Returns current client's index.
    // -1 if list is empty
    
    void forward(); 
    // Advance current client one forward.
    // Back to first if beyond the end, so behaves like a circular list.
    
    void backward();
    // Current client index goes one backwards.
    // If current was the first, sets the last as current.

    void reset();
    // Sets current client to first. If list is empty, does nothing.

    uint8_t items();
    // Number of items present in the list.

    // List querying
    
    long value();
    // Return current client's value

    // Return value's whole and decimal part.
    // Wole part is with its sign, decimal part is always positive.
    long euros();
    long cents();
        
    const char * name();
    // Return a null terminated string holding current client's name.
    // Maximum CLIENT_NAME_LENGTH + 1 bytes including '\0';

    char mark();
    // Return current client's status mark (unvisited, visited, edited);

    void setMark(char c);
    // Set current client's status mark (either visited, unvisited, edited);

    void setValue(long num);
    // Sets current client's value to the given number.
    
    bool isFull();
    // Returns true if list if full, otherwise, false.
    
    bool isEmpty();
    // true if list is empty, false otherwise.

    // Constructors and destructors

    int addItem(char * name, long value);
    // Add a new client to the end of the list.
    // Added item becomes current item.
    // Returns 0 (false) if list is alredy full.
    
    int delItem(); 
    // Delete current client, and shifts up the rest from the end. 
    // Return 0 if list is empty
  
  private:
    ClientItem _list[LIST_SIZE]; // Client list
    int8_t _last, _current; // list pointers. Signed to use -1 as a mark.
};

#endif
