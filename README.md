## ArduCASH

Copyright 2021 by Jaume Batlle Brugal. See [LICENSE](#license) for more info.

ArduCASH is a scketch for the Arduino platform that implements a
cash register with records for up to 50 clients. Each client has a
name field of 15 characters, and a value field which holds the client's
balance.

It features a calculator (simple, only add and subtract) and two
programmable keys.


### HARDWARE AND WIRING

To implement this kit you will need:

   - An **Arduino UNO** board or compatible - beware of EEPROM size of the
      board. The original program needs 1024 bytes of EEPROM size.
   - A **VMA203** shield, with an LCD of 16 x 2 characters and 6 buttons.
   - A **matrix keypad** of 4x4 keys. A good quality one is preferable.
   - 8 wires to connect the keypad to the shield.

You may want also to provide a box to enclose your ArduCASH. Bear in mind
that the VMA203 shield has a potentiometer to adjust screen's contrast that
sometimes is too tall for a box. You can either make room for it in your
design, or remove it and solder it again but _under_ the board.

There is a `case` folder with several .STL files with a proposed design
for you to use, if you wish. Casing design is made by Andreu Santacana.

The shield is attached to the UNO in its intended position.
Keypad pads are wired to pins 2, 3, 11, 12, A1, A2, A3 and A4, respectively,
from left to right.

The keypad of 4x4 keys has digits `0` to `9`, letters `A` to `D`,
and `#` and `*` keys. You may want to label those keys according to the
following, to be consistent with this short manual:

    Key  |  Label
    -----+-------
      A  |   +/=
      B  |   -
      C  |   C
      D  |   OK (or the 'Return' arrow)
      *  |   P1
      #  |   P2


### INSTALLATION

The easiest way to install this sketch to your Arduino board is by having the
Arduino IDE installed (visit [Arduino.cc](<https://www.arduino.cc/en/software>)
to see how to do it).


#### Dependencies

This sketch uses the `Keypad.h` and `LiquidCrystal.h` libraries, so make sure
you have them both installed. You could do so within the same Arduino IDE.

You will also need a Python3 interpreter to upload some useful data to ArduCASH,
with the PySerial module (`pip install pyserial`).


#### Obtaining the sources

You can download this project directly from
[GitLab](<https://gitlab.com/jbatlle/arducash/-/archive/master/arducash-master.zip>) in a .zip file.

Unzip the file in a folder inside your 'Arduino' projects folder.

If you have `git` installed in your system, you may prefer to _clone_ this
project. Simply go to the `Arduino` folder (where Arduino IDE stores its
sketches) and execute:

    git clone git@gitlab.com:jbatlle/arducash.git

Once the files are downloaded, open the Arduino IDE and open the
`ArduCASH.ino` file. You should have the project ready to compile.

Type `Ctrl-R` to compile the project.

Type `Ctrl-U` to compile and upload the sketch to the board. The board must
be connected to the computer to upload the sketch there, of course!


#### Add some useful information

Once the sketch is uploaded to the board, it will show an \<empty\> list
message. You would be able to activate the calculator by pressing `C` and
to program `P1` and `P2` values (asterisk `*` and hash `#` keys in the
keypad), and that's all.

You will need to upload the list of _clients_ you wish to track into the
board. To do so, there's a small utility called `ac-backup.py` written in
Python that lets you do so.

First, prepare a comma separated list of names and monetary values in a file.
The format of the file is \<string\>,\<coin\>.\<cents\> for each line.

Example:

```
John Doe,-7.30
Anne Bee,0.00
Mary Joseph,5.00
```

ArduCASH cand hold up to 50 records, and for each name, only the first 15
characters are used.

Names are not compared, each register is associated to a vector position, so,
eventually, all names could be just the same!

Once you have your file ready, and named accordingly (say, `file.csv`, for
example), you can upload it to the board by invoking:

    python3 ac-backup.py -r file.csv

ArduCASH should restart and show an informative message on the screen. Then,
the data should be loaded to the board.

If you get the error `pyserial is not found`, you should install it. The easiest
way, is by invoking `pip` in this way:

     pip install pyserial

If this doesn't work, try:

     python -m pip install pyserial

If you need further help, visit [PySerial's website](https://pythonhosted.org/pyserial/).


###  USE

The use is quite straightforward, but there are some things that must be
known, so there is a brief explanation of how to opearte it.


### Simple calculus: the calculator

To activate the calculator, press the `C` key.
This is a quite simple calculator, which can add and subtract. Every time
you enter a number, press `+/=` key to see the result.
Pressing `P1` or `P2` **adds** their value to the count, so, if you want
their value to be subtracted, enter a negative value for them (see below).

To exit the calculator, press either `C` or `OK`. As value is not stored
anywhere, this makes no difference.


#### Programming P1 and P2 keys

When you are in the _list view_, press `P1` or `P2` to edit their values.
This will show the message _'Enter a new value for Px'_ and activate the
calculator.

Once the value is set, press `OK` to store it.
If you press `C`, the value will be set to 0.00 (think of it as _'Clear'_).


### Navigating client's list

You can navigate the client's list by pressing the `+/=` and the `-` keys, or
the `UP` and `DOWN` buttons under the screen.

In front of currently selected client there is a _state character_ that
indicates, for the current client, and for current session (since last restart)
if client has been:

  - '*' accessed; this is, asked for its balance.
  - '#' edited; this is, entered calculator mode for the client.
  - '>' unaccessed: none of the previous two states.

All clients start in a '>' (unaccessed) state.


#### Operating on a client

When you are on an item from the list, press the `OK` key. The name of the
client will be shown in the top line of the screen and you will enter the
calculator.

Once the calculus are performed, press `OK` to confirm and store the new
balance value for that client, or `C` to abort and keep last balance. If
you want to set its new balance to 0.00, meaning that it pays for all what's
owed, press `SELECT`.

Client's status will change to 'edited' ('#') even if the operation was
cancelled.

If you just want to see the client's balance, just press `SELECT` while
in _list view_. In this case, client's state will change to 'visited' ('*').


#### Screen brightness

If you want to adjust the LCD screen brightness, press `LEFT` or `RIGHT`
buttons under the screen to decrease or increase it.


#### Starting over

The rightmost button under the screen is a `RESET` button. In normal
operation, the program shouldn't hang, but the button is there, just in
case.


### WARNING

This sketch makes use of Arduino's internal EEPROM to store data. This
EEPROM has a limited amount of write operations that can be performed on
it. According to ATMEL information, about 100,000 write cycle/cell.

The use that this sketch makes of EEPROM storage is not intensive, so
it shuld last for **years** before reaching its write limits (writing
a user's data 3 times a day would take about 91 years to reach to 100,000).

That should be fine, but is an information that is worth knowing.


### LICENSE

Copyright 2021 by Jaume Batlle Brugal

This file is part of **ArduCASH**.

ArduCASH is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ArduCASH is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a [copy](./COPYING.txt) of the GNU General Public License
along with ArduCASH. If not, see
[<http://www.gnu.org/licenses/>](<http://www.gnu.org/licenses/>).
