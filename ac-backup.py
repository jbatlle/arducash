#! /usr/bin/python3
# Small backup tool for ArduCASH
#
# Copyritht (C) 2021 Jaume Batlle Brugal
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import serial.tools.list_ports
import serial
import os.path
import sys
import time

def byteToText(bytestream):
    """ Transform a byte stream into a text.
    First try to encode it as ascii, which should be the encoding,
    but in case it fails, it degrades to latin1, which is a superset
    of ascii and has all possible byte values encoded.
    So the most we'll get is a string of strange characters.
    """
    
    try:
        text = str(bytestream, encoding='ascii')
    except:
        text = str(bytestream, encoding='latin1')

    return text


def openArduinoPort(port_name = None):
    """
    Try to locate a port with an Arduino board attached to.
    Return the opened port, or None on failure.
    If a port_name is given use it instead of guessing.
    """
    
    ap = None # Resulting port

    if port_name == None:
        # Try to guess a port
        
        ports = list(serial.tools.list_ports.comports())
    
        for p in ports:
         
            if "Arduino" in p.description:
                port_name = p.device
            else:
                print("**Found device:")
                print("  Desc: ", p.description)
                print("  Device: ", p.device)

    if port_name != None:
        try:
            ap = serial.Serial(port_name
                               , baudrate=9600
                               , timeout=5.0
                               , write_timeout = 2.5)
        except:
            ap = None

    return ap


def dumpEEPROM(device):

    print("Waiting for ArduCASH to restart...")
    time.sleep(6.0)
    
    if device.in_waiting > 0:
        device.reset_input_buffer()
        time.sleep(0.5)
    
    print("Dumping ArduCASH EEPROM contents:")
    device.write(b'D') # Tell ArduCASH to dump EEPROM

    data = device.read(2048) # up to 2kB, in 5 sec
    print(byteToText(data)) # show in screen


def backup(device, filename = None): 

    print("Waiting for ArduCASH to restart...")
    time.sleep(6.0)
    
    if device.in_waiting > 0:
        device.reset_input_buffer()
        time.sleep(0.5)
    
    print(f"Backing up ArduCASH to {filename}.")
    device.write(b'B') # Tell Arduino to send data

    with open(filename, 'wb') as fp:
        data = device.read(2048) # up to 2kB, in 5 sec
        print(byteToText(data)) # show in screen
        fp.write(data) # and write to file


def restore(device, filename = None):

    print("Waiting for ArduCASH to restart...")
    time.sleep(6.0)
    
    if device.in_waiting > 0:
        device.reset_input_buffer()
        time.sleep(0.5)

    if not os.path.exists(filename):
        print(f"{filename} not found. Please, check spelling.")
        exit(1)
        
    print(f"Restoring from {filename} to ArduCASH.")

    with open(filename, 'rb') as fp:
        device.write(b'R') # Tell Arduino to prepare to receive data
        data = fp.read()
        time.sleep(0.5)
        device.write(data)
        
        time.sleep(0.5)

        while device.in_waiting > 0:
            print(byteToText(device.read(device.in_waiting)))
            time.sleep(0.5)
    
    
helptext = """
Usage:  ac-backup -b|-r filename [device]
          or
        ac-backup -d [device]
        
  -b: Backup. Given filename will be overriden by ArduCASH contents.
  -r: Restore. Contents of filename will be loaded to ArduCASH.
  -d: Debug. Prints EEPROM contents in a somewhat readable form.

  filename: CSV file containing <string>,<value> pairs.
    i.e:
       name one,-5.00
       name two,0.00
       ...

   [device]: optional field that indicates where the Arduino is connected
       to (COM3, /dev/ttyACM0, etc.)

    Take into account that only the 15 first characters of each string
    will be used.
    The screen only accepts ASCII characters.
"""


def main():
    if len(sys.argv) < 2:
        print("ERROR: Incorrect number of arguments")
        print(helptext)
        exit(1)
    
    if sys.argv[1] == "-d":
        # dump EEPROM contents to screen
        print("Looking for Arduino...")

        if len(sys.argv) == 3:
            arduino = openArduinoPort(sys.argv[2])
        else:
            arduino = openArduinoPort() # try to autodetect arduino port
        
        if arduino == None:
            print("Could not open Arduino.")
            exit(1)

        dumpEEPROM(arduino)
        
    elif sys.argv[1] == "-r":
        print("Looking for Arduino...")
        
        if len(sys.argv) == 4:
            arduino = openArduinoPort(sys.argv[3])
        elif len(sys.argv) == 3 :
            arduino = openArduinoPort()
        else:
            print("ERROR: Incorrect number of arguments")
            exit(1)

        if arduino == None:
            print("Could not open Arduino.")
            exit(1)

        restore(arduino, sys.argv[2])
        
    elif sys.argv[1] == "-b":
        print("Looking for Arduino...")
        
        if len(sys.argv) == 4:
            arduino = openArduinoPort(sys.argv[3])
        elif len(sys.argv) == 3 :
            arduino = openArduinoPort()
        else:
            print("ERROR: Incorrect number of arguments")
            exit(1)

        if arduino == None:
            print("Could not open Arduino.")
            exit(1)

        backup(arduino, sys.argv[2])
                    
                    
main()
